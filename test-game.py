import kivy

kivy.require('1.9.0')

from kivy.app import App
from kivy.core.window import Window
Window.clearcolor = (1, 1, 1, 1)

from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.codeinput import CodeInput
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.widget import Widget

from kivy.extras.highlight import PythonLexer

import etagame

class GameWidget(Widget):
    """The game display area.

    Will eventually contain game visuals
    """
    def __init__(self, **kwargs):
        """Initialise the GameWidget."""
        super(GameWidget, self).__init__(**kwargs)
        self.add_widget(Label(
            text='ayy lmao',
            color=(0, 0, 0, 1)
        ))

class CodeButtonLayout(GridLayout):
    """The container for the code evaluate/clear buttons.

    Contains two buttons:
    - eval_code  :: will attempt to evaluate the user input
    - clear_code :: will clear the input area
    """
    def __init__(self, **kwargs):
        """Initialise the CodeButtonLayout.

        """
        super(CodeButtonLayout, self).__init__(**kwargs)

        self.cols = 2
        self.padding = 10

        self.eval_code = Button(text='Evaluate')
        self.clear_code = Button(text='Clear')

        self.add_widget(self.eval_code)
        self.add_widget(self.clear_code)

class InputAreaLayout(GridLayout):
    """The container for the code input area.

    Contains the code input area and evaluation/clear buttons
    """
    def __init__(self, **kwargs):
        """Initialise the InputAreaLayout."""
        super(InputAreaLayout, self).__init__(**kwargs)

        self.cols = 1
        self.padding = 10

        self.game_info = Label(
            text='Foo',
            size_hint_y=None,
            height=200,
            color=(0, 0, 0, 1)
        )
        self.code_input = CodeInput(
            lexer=PythonLexer(),
            style='colorful'
        )
        self.code_buttons = CodeButtonLayout(size_hint_y=None)

        self.code_buttons.eval_code.bind(on_press=self.eval_code)
        self.code_buttons.clear_code.bind(on_press=self.clear_code)

        self.add_widget(self.game_info)
        self.add_widget(self.code_input)
        self.add_widget(self.code_buttons)

        self.add_skeleton()

    def eval_code(self, instance):
        """Evaluate the user input.

        Returns immediately if nothing is provided

        This function adds the following to the user input before running:
            - Library inputs and initialisation
            - A runner function

        Any errors that occur are simply printed to the console but will
        eventually be printed to a dedicated output area in the layout.
        """
        code = self.code_input.text
        if not code:
            return

        try:
            result = etagame.EtaEvaluator(code).evaluate()
            self.game_info.text = result
        except Exception, e:
            print(e.message)

    def clear_code(self, instance):
        """Reset the code input area."""
        self.add_skeleton()

    def add_skeleton(self):
        """Add the skeleton code needed for this level."""
        code = """def solution(G):
    \"\"\"Do the thing.\"\"\"
    return 'Nothing yet'
"""
        self.code_input.text = code

class GameLayout(GridLayout):
    """The main layout for the game.

    Contains the game widget and input area
    """
    def __init__(self, **kwargs):
        """Initialise the GameLayout."""
        super(GameLayout, self).__init__(**kwargs)
        self.cols = 2

        self.game_widget = GameWidget(clearcolor=(1, 1, 1, 1))
        self.input_area = InputAreaLayout()

        self.add_widget(self.game_widget)
        self.add_widget(self.input_area)

class GameApp(App):
    """The game application."""
    def build(self):
        """Create and return the root layout."""
        return GameLayout()

if __name__ == '__main__':
    GameApp().run()
