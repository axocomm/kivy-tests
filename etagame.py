import re
import sys

from cStringIO import StringIO

class EtaEvaluator:
    """ETA code evaluator."""
    HEADER = ['']
    FOOTER = [
        'def main():',
        '    print(solution(None))',
        ''
        'main()'
    ]

    class CodeBuilder:
        @classmethod
        def build(cls, **kwargs):
            if 'body' not in kwargs:
                raise Exception('Missing body')

            header = footer = []
            body = kwargs['body']
            if 'imports' in kwargs:
                header += kwargs['imports']

            if 'runner' in kwargs:
                footer += kwargs['runner']

            print(header)

            h = '\n'.join(header)
            f = '\n'.join(footer)

            return '\n'.join([h, body, f])

        @classmethod
        def check_input(cls, code):
            """Check input code for obvious problems."""
            if re.search('while True', code):
                raise Exception('Very funny')
            return True

    def __init__(self, code):
        """Initialise the EtaEvaluator."""
        self.code = code

    def evaluate(self):
        """Add boilerplate and evaluate the given code."""
        try:
            to_run = self.add_boilerplate(self.code)
            print(to_run)

            compiled = compile(to_run, '<string>', 'exec')

            old_stdout = sys.stdout
            new_stdout = sys.stdout = StringIO()
            exec(compiled, {'__name__': '__main__'})
            sys.stdout = old_stdout

            return new_stdout.getvalue()
        except SyntaxError:
            print('Got syntax error')
        except Exception, e:
            print('Got error: %s' % (e.message))

    @classmethod
    def add_boilerplate(cls, code):
        """Add header and footer to code.

        This will eventually add required libraries and
        bootstrapping to be done prior to evaluation.
        """
        return cls.CodeBuilder.build(
            body=code,
            imports=[],
            footer=cls.FOOTER
        )
